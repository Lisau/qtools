#include "ptable.h"
#include "include.h"
#include <stdint.h>

int main(int argc, char **argv)
{
	if (argv[1] == NULL) {
		printf("\nНе указан файл\n");
		return 1;
	}
	FILE* in = fopen(argv[1], "rb");
	if (in == NULL) {
		printf("\n Cannot open file %s\n",argv[1]);
		return 2;
	}
	printf ("Чтение: %d\n", load_ptable_file(argv[1]));
	list_ptable();
	return 0;
}
