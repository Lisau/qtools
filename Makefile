CC = gcc
CFLAGS = -std=c99 -O2 -g2 -Wall
LDFLAGS = -lpthread
SOURCES = chipconfig.c  hdlc.c  main.c  memio.c  ptable.c  qcio.c
OBJECT_FILES = $(addprefix obj/, $(SOURCES:.c=.o))
EXECUTABLE = part

all: obj $(SOURCES) $(EXECUTABLE)

obj:
	mkdir -p obj

$(EXECUTABLE): $(OBJECT_FILES)
	$(CC) $(OBJECT_FILES) $(LDFLAGS) $(CFLAGS) -o $@

obj/%.o: %.c
	$(CC) -c $< $(CFLAGS) -o $@

clean:
	rm -rf obj $(EXECUTABLE)
